<?php

namespace Tests\Feature;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TypesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_all_types_route_has_been_successfuly_rendered()
    {
        $response = $this->get('/types');

        $response->assertStatus(200);
    }

    public function test_new_type_can_be_created()
    {
        $response = $this->post('store-type-form', [
            'name' => 'Petit test'
        ]);
        $response->assertRedirect('/articles');
    }
}
