<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>Coucou ! clique <a href="/articles/create">ici</a> pour créer un nouvel Article</p>
    <div id="articles">
        @foreach ($articles as $article)
        <div id="article-{{$article->id}}">
            <h1>{{ $article->name }}</h1>
            @if ($article->types != null)
            <p>Types:</p>
            <p>=> {{$article->types->name}}</p>
            @endif
            @if ($article->colors != null)
            <p>Couleurs:</p>
            @foreach ($article->colors as $color)
            <p>{{$color->name}}</p>
            @endforeach
            @endif
            <a href="/articles/edit/{{ $article->id }}">Modifier</a>
            <a href="/articles/delete/{{ $article->id }}">Supprimer</a>
        </div>
        @endforeach
    </div>
</body>
</html>