<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form name="article" id="article" method="POST" action="{{url('store-article-form')}}">
{{ csrf_field() }}
<div class="form-group">
<label for="name">Name</label>
<input type="text" id="name" name="name" class="@error('name') is-invalid @enderror form-control">
<!-- START TYPES -->
<br>
<label for="types">Types<label>
<br>
<select name="types" id="types">
@foreach ($types as $type)
<option value="{{$type->id}}">{{$type->name}}</option>
@endforeach
</select>
<!-- END TYPES-->
<!-- START COLORS-->
<br>
<label for="colors">Couleurs </label>
<br>
<select name="colors[]" id="colors[]" multiple>
@foreach ($colors as $color)
<option value="{{$color->id}}">{{$color->name}}</option>
@endforeach
</select>
<!-- END COLORS-->
@error('name')
<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
@enderror
</div>
<button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html>