<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>Coucou ! clique <a href="/colors/create">ici</a> pour créer une nouvelle couleur</p>
    <div id="colors">
        @foreach ($colors as $color)
        <div id="color-{{$color->id}}">
            <h1>{{ $color->name }}</h1>
            <a href="/colors/edit/{{ $color->id }}">Modifier</a>
            <a href="/colors/delete/{{ $color->id }}">Supprimer</a>
        </div>
        @endforeach
    </div>
</body>
</html>