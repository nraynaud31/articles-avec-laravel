<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<p>Coucou ! clique <a href="/types/create">ici</a> pour créer un nouveau Type</p>
    <div id="types">
        @foreach ($types as $type)
        <div id="type-{{$type->id}}">
            <h1>{{ $type->name }}</h1>
            <a href="/types/edit/{{ $type->id }}">Modifier</a>
            <a href="/types/delete/{{ $type->id }}">Supprimer</a>
        </div>
        @endforeach
    </div>
</body>
</html>