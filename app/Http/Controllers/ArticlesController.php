<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Articles;
use App\Models\Types;
use App\Models\Colors;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Articles::all();
        return view('articles', ['articles'=>$articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $types = Types::all();
        $colors = Colors::all();
        return view('articles-create', ['types'=>$types, 'colors'=>$colors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
        ]);

        $articles = new Articles;
        $articles->name = $request->name;
        $articles->type_id = $request->types;
        $articles->save();

            // dd($request->colors[$i]);
            $articles->colors()->attach($request->colors);

        return redirect('/articles')->with('status', 'Form Data Has Been Inserted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $articles = Articles::find($id);
        $types = Types::all();
        $colors = Colors::all();
        return view('articles-update', ['articles'=>$articles, 'types'=>$types, 'colors'=>$colors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Je vérifie que tous les champs sont remplis
        $this->validate($request, [
            'name' => 'required',       
        ]);

        // Je mets à jour les data et enregistre dans la DB
        $articles = Articles::find($id);
        $articles->name = $request->get('name');
        $articles->type_id = $request->get('types');
        $articles->save();

        $articles->colors()->detach();

        $articles->colors()->attach($request->colors);

        return redirect('/articles')->with('success', 'Blog Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $article = Articles::FindorFail($id);
        $article->delete();
        return redirect()->action([ArticlesController::class, 'index']);
    }
}
