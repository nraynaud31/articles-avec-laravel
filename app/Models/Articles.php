<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    use HasFactory;
    
    protected $table = 'articles';

    // Propriétés du model
    protected $fillable = [
        'name',
        'type_id',
    ];

    public function types(){
        return $this->belongsTo(Types::class, 'type_id');
    }

    public function colors(){
        return $this->belongsToMany(Colors::class);
    }

}
