<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line

class Types extends Model
{
    use SoftDeletes; //add this line
    use HasFactory;


    protected $table = 'types';

    // Propriétés du model
    protected $fillable = [
        'name'
    ];

    public function articles(){
        $this->hasMany(Articles::class);
    }
}
