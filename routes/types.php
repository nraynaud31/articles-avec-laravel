<?php

use App\Http\Controllers\TypesController;
use Illuminate\Support\Facades\Route;

Route::controller(TypesController::class)->group(function () {
    Route::post('store-type-form', 'store');
    Route::get('/types/delete/{id}', 'destroy');
    Route::get('/types', 'index');
    Route::post('/types/update/{id}', 'update');
    Route::get('/types/create', 'create');
    Route::get('/types/edit/{id}', 'edit');
});