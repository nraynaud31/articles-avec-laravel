<?php

use App\Http\Controllers\ColorsController;
use Illuminate\Support\Facades\Route;

Route::controller(ColorsController::class)->group(function () {
    Route::post('store-color-form', 'store');
    Route::post('/colors/update/{id}', 'update');
    Route::get('/colors/delete/{id}', 'destroy');
    Route::get('/colors', 'index');
    Route::get('/colors/create', 'create');
    Route::get('/colors/edit/{id}', 'edit');
});