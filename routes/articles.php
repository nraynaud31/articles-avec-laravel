<?php

use App\Http\Controllers\ArticlesController;
use Illuminate\Support\Facades\Route;

Route::controller(ArticlesController::class)->group(function() {
    Route::post('store-article-form', 'store');
    Route::get('/articles/delete/{id}', 'destroy');
    Route::post('/articles/update/{id}', 'update');
    Route::get('/articles', 'index');
    Route::get('/articles/create', 'create');
    Route::get('/articles/edit/{id}', 'edit');
});